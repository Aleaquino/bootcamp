﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigos: MonoBehaviour
{
    [SerializeField] float speed = 5f;
    private bool horizontal = false;
    private Vector3 currentPosition;

    void Update()
    {


        if (!horizontal)
        {


            if (transform.position.x <= 21.4)
            {
                currentPosition = transform.position;
                currentPosition.x += 1 * speed * Time.deltaTime;
                transform.position = currentPosition;



            }
            else
            {
                transform.position += Vector3.down;
                horizontal = true;

            }
        }
        else
        {
            if (transform.position.x >= -21.4)
            {
                currentPosition = transform.position;
                currentPosition.x -= 1 * speed * Time.deltaTime;
                transform.position = currentPosition;
            }
            else
            {
                transform.position += Vector3.down;
                horizontal = false;

            }



        }


    }
}
