﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeController: MonoBehaviour
{
    [SerializeField] private int arbolCantidad = 20;
    [SerializeField] private GameObject[] trunk;
    [SerializeField] private GameObject arbolPrefab;
    [SerializeField] private Transform posicionDeCreacion;
    private Vector3 posicionTemp;
    void Awake()
    {
        InicioAleatorio();
    }

    void InicioAleatorio()
    {
        int numeroRandom = Random.Range(10, arbolCantidad + 1);

        trunk = new GameObject[numeroRandom];

        float cont = -7.65f;

        for (int i = 0; i < numeroRandom; i++)
        {
            posicionTemp = posicionDeCreacion.transform.position;
            posicionTemp.y = transform.position.y + cont;
            GameObject arbol = GameObject.Instantiate(arbolPrefab, posicionTemp, Quaternion.Euler(-90f, 0f, 0f));
            arbol.transform.parent = this.transform;

            cont += 0.75f;
        }
    }

    void Update()
    {

    }
}