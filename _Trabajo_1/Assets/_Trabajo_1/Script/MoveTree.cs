﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTree : MonoBehaviour
{
    [SerializeField] private GameObject trunk;

    void Start()
    {
        LeanTween.moveY(trunk, 2f, 1f);
    }


}
